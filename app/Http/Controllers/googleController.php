<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Google_Service_Calendar_Resource_Freebusy;
use Google_Service_Calendar_FreeBusyRequest;
use DateTime;
use Google_Service_Calendar_FreeBusyRequestItem;
class googleController extends Controller
{

    protected $client;
    // public function __construct()
    // {
    //     $client = new Google_Client();
    //     $client->setAuthConfig('client_secret.json');
    //     $client->addScope(Google_Service_Calendar::CALENDAR);
        // $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        // $client->setHttpClient($guzzleClient);
    //     $this->client = $client;
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client = new Google_Client();
            $client->setAuthConfig('client_secret.json');
            $client->addScope(Google_Service_Calendar::CALENDAR);

            $client->setAccessType("offline");        // offline access
            $client->setIncludeGrantedScopes(true);   // incremental auth

            $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
            $client->setHttpClient($guzzleClient);

            $cal = new Google_Service_Calendar($client);
            $freebusy_req = new Google_Service_Calendar_FreeBusyRequest();
            $freebusy_req->setTimeMin(date(DateTime::ATOM, strtotime('2017-05-01T08:00:00.000Z')));
            $freebusy_req->setTimeMax(date(DateTime::ATOM, strtotime('2017-05-19T17:00:00.000Z')));
            $freebusy_req->setTimeZone('Europe/London');
            $freebusy_req->setCalendarExpansionMax(10);
            $freebusy_req->setGroupExpansionMax(10);
            
            $item = new Google_Service_Calendar_FreeBusyRequestItem();
            $item->setId('primary');
            $freebusy_req->setItems(array($item));  
            $query = $cal->freebusy->query($freebusy_req);
            $response_calendar = $query->getCalendars();
            dd($response_calendar);
        } else {
            return redirect()->route('oauthCallback');
        }
    }

    public function oauth()
    {
        session_start();
        $rurl = action('googleController@oauth');
        $this->client->setRedirectUri($rurl);
        if (!isset($_GET['code'])) {
            $auth_url = $this->client->createAuthUrl();
            $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);
            return redirect($filtered_url);
        } else {
            $this->client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $this->client->getAccessToken();
            return redirect()->route('google.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
